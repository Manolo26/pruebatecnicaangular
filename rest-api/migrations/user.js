exports.up = function(knex) {
  return knex.schema.createTable('user', table => {
    table.increments('id').primary();
    table.string('name', 100);
    table.date('birth');
    table.string('username',100);
    table.string('job',100);
    table.string('email',100);
    table.datetime('date_create').defaultTo(knex.fn.now());
  });
};
exports.down = function(knex) {
  return knex.schema.dropTableIfExists('user');
};