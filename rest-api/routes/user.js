const Users = require('../models/Users');
const validate = require('./interceptor');
const AR = require('../ApiResponser');
const upload = require('multer')();
const router = require('express').Router();
const sanitizeString = require('../functions/sanitizeString');
const { raw } = require('objection');

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_SERVER_ERROR = 500

router.get('/', /*validate('users'),*/ async(req, res) => {
  const params = req.query;
  const Query = Users.query();

  if (+params.page === +params.page)
    Query.page(parseInt(params.page), +params.items ? parseInt(params.items) : 10)

  if (params.filter){
    if (String(params.filter).length > 1) {
      Query.having('name', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('username', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('job', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('email', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    }
  } 
  await Query
    .then(async resp => {
      const Total = await Users.query().count();
      const data = {
        total: Total[0]['count(*)'],
        data: resp.results
      }
      AR.sendData(data, res);
    })
    .catch(err => {
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/home', /*validate('users'),*/ async(req, res) => {
  const params = req.query;
  const Query = Users.query().select('job',raw('count(*)').as('total') ).groupBy('job')
  
  await Query
    .then(async resp => {
      const Total = await Users.query().count();
      
      let data2 = {
        labels: [],
        totals: []
      };
      for (let index = 0; index < resp.length; index++) {
        const e = resp[index];
        data2.labels.push(`${e.job}`);
        data2.totals.push(e.total);
      }
      const data = {
        total: Total[0]['count(*)'],
        data: resp,
        data2: data2
      }
      AR.sendData(data, res);
    })
    .catch(err => {
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.post('/', upload.none(), /*validate('users'),*/ async(req, res) => {
  const data = req.body;

  let errr = false;
  if (!data.name) errr = "Name is required";
  else if (!data.birth) errr = "Date of birth is required";
  else if (!data.username) errr = "Username is required";
  else if (!data.job) errr = "Job is required";
  else if (!data.email) errr = "Email is required";
  else {
    const search = await Users.query().findOne({username: data.username}).catch(console.error);
    if (search)
      errr = 'User already exists';
    const search_2 = await Users.query().findOne({email: data.email}).catch(console.error);
    if (search_2&&!errr)
      errr = 'Email already exists';
  }

  if (errr)
    return AR.sendError(errr, res, HTTP_BAD_REQUEST);


  const Set = {
    name: data.name,
    birth: data.birth,
    username: data.username,
    job: data.job,
    email: data.email
  };


  await Users.query()
    .insert(Set)
    .then(async resp => {
      if (!resp)
        return res.sendStatus(HTTP_NOT_FOUND);
      resp.r = true;
      resp.msg = "Success";
      AR.sendData(resp, res);
    })
    .catch(err => {
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.put('/', upload.none(), /*validate('users'),*/ async(req, res) => {
  const data = req.body;
  console.log(data);
  let errr = false;
  if (!data.id) errr = "Id is required";
  else if (!data.name) errr = "Name is required";
  else if (!data.birth) errr = "Date of birth is required";
  else if (!data.username) errr = "Username is required";
  else if (!data.job) errr = "Job is required";
  else if (!data.email) errr = "Email is required";
  else {
    const search = await Users.query().findOne({username: data.username}).whereNot('id', data.id).catch(console.error);
    if (search)
      errr = 'User already exists';
    const search_2 = await Users.query().findOne({email: data.email}).whereNot('id', data.id).catch(console.error);
    if (search_2&&!errr)
      errr = 'Email already exists';
  }

  if (errr)
    return AR.sendError(errr, res, HTTP_BAD_REQUEST);

  const Set = {
    name: data.name,
    birth: data.birth,
    username: data.username,
    job: data.job,
    email: data.email
  };

  await Users.query()
    .patchAndFetchById(data.id, Set)
    .then(async resp => {
      if (!resp)
        return res.sendStatus(HTTP_NOT_FOUND);
      resp.r = true;
      resp.msg = "Success";
      AR.sendData(resp, res);
    })
    .catch(err => {
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.delete('/:id(\\d+)', /*validate('users'),*/ async(req, res) => {
  const data = req.params;
  await Users.query()
  .findById(data.id)
  .del()
  .then(async resp => {
    if (!resp)
      return res.sendStatus(HTTP_NOT_FOUND);
    AR.sendData({r:true}, res);
  })
  .catch(err => {
    res.sendStatus(HTTP_SERVER_ERROR);
  })   
});

module.exports = router;
