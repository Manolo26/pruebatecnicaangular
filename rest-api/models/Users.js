"use strict";
const { Modelo } = require("./Modelo");

class Users extends Modelo {
  static get tableName() {
    return "user";
  }
}

module.exports = Users;
