const Knex = require("knex");
const morgan = require("morgan");
const express = require("express");
const promiseRouter = require("express-promise-router");
const bodyParser = require("body-parser");
const cors = require('cors');
const os =  require("os");
const Server = os.hostname();
const knexConfig = require("./knexfile.js");
const https = require("https");
const fs = require("fs");

const { Model } = require("objection");

var knex;

knex = Knex(knexConfig.development);

Model.knex(knex);

const router = promiseRouter();

const app = express()
  .use(express.json({limit: '999mb'}))
  .use(cors())
  .use(bodyParser.urlencoded({ extended: true }))
  .use(bodyParser.json())
  .use(morgan("dev"))
  .use(router)
  .set("json spaces", 2);
  app.all('*', function(req, res,next) {
    var responseSettings = {
      "AccessControlAllowOrigin": req.headers.origin,
      "AccessControlAllowHeaders": "Content-Type,X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name",
      "AccessControlAllowMethods": "POST, GET, PUT, DELETE, OPTIONS",
      "AccessControlAllowCredentials": true
    };
    res.header("Access-Control-Allow-Credentials", responseSettings.AccessControlAllowCredentials);
    res.header("Access-Control-Allow-Origin",  responseSettings.AccessControlAllowOrigin);
    res.header("Access-Control-Allow-Headers", (req.headers['access-control-request-headers']) ? req.headers['access-control-request-headers'] : "x-requested-with");
    res.header("Access-Control-Allow-Methods", (req.headers['access-control-request-method']) ? req.headers['access-control-request-method'] : responseSettings.AccessControlAllowMethods);

    if ('OPTIONS' == req.method) {
      res.sendStatus(200);
    }
    else {
      next();
    }
});

Routes([
  'user'
]);

var server = app.listen(knexConfig.PORT, () => {
  console.log(
    "API run in Port: %s",
    server.address().port
  );
});


function Routes(rutas) {
  rutas.forEach(ruta => {
    app.use(`/${ruta}`, require(`./routes/${ruta}`));
  });
}