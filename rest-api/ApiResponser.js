class ApiResponser {
    static sendData(data, res) {
        return res.status(200).send(data);
    }

    static sendMessage(msg, res) {
        return res.status(200).send({
            msg: msg,
            code: 200
        });
    }

    static sendAuth(res, status, token = null, user = null) {
        if (!status)
            res.status(401).send({
                auth: false,
                token: null
            });
        else
            res.status(200).send({
                auth: true,
                token: token,
                user: user
            });
    }

    static sendError(msg, res, code) {
        return res.status(code).send({
            msg: msg
        });
    }
}

module.exports = ApiResponser;
