// Update with your config settings.

module.exports = {
  PORT: 3000,
  Hosname: 'server.dev',
  development: {
    client: 'mysql2',
    connection: {
      database: 'webtest',
      user:     'root',
      password: '',
      host: 'localhost',
      charset: 'utf8mb4'
    },
    migrations: {
      directory: "./migrations"
    },
    seeds: {
      directory: "./seeds"
    },
  }


};
