
# Prueba tecnica Angular / NodeJS

Aplicacion web, dónde el front-end será en Angular y el back-end NodeJS. Para la base de datos se uso MySQL.

### Versiones:

* Angular CLI: 13.1.1
* Node: 16.13.0
* Mysql 15.1 Distrib 10.1.13-MariaDB, for Win32 (AMD64)

> Nota: Para cargar el archivo .sql que se encuentra en la raíz del proyecto debe crear la base de datos en mysql con el nombre **"_webtest_"** y proceder a importar el script.

### En la carpeta (rest-api/)

Se deben ejecutar los siguientes comandos:

`npm install --force` (se ejecuta por primera ves antes de correr el api y cuando se anexan nuevas dependencias)

Para correr el api
```j
$ npm run start
```

### En la carpeta (app/)

Se deben ejecutar los siguientes comandos:

`npm install --force` (se ejecuta por primera ves antes de correr el api y cuando se anexan nuevas dependencias)

Para iniciar el servidor
```j
$ npm run start
```

Para ver en el projeto toque [aqui](http://localhost:4200)
---

Si se desea compilar el la web para producción debe realizarse el siguiente comando:

```j
$ npm run build
```