import { Component, OnInit } from '@angular/core';
import { UserService } from "src/app/shared/services/user.service";
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  total: number = 0;
  job: any = [];
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [['']];
  public pieChartData: SingleDataSet = [0];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public alls: number = 0;

  constructor(private userService: UserService) {
  	monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit(): void {
  	this.get();
  }
  get() {
    
    this.userService.gethome()
      .subscribe((trae: any) => {
      	console.log(trae);
      	this.pieChartLabels = trae.data2.labels;
     	  this.pieChartData = trae.data2.totals;
     	  this.alls = trae.data2.labels.length + trae.data2.totals.length; 
       	this.total = trae.total || 0;
       	this.job = trae.data || [];
      });
  }
}
