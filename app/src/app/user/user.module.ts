import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { UserComponent } from "./containers/user/user.component";
import { UserRoutingModule } from "./user-route.routing";

@NgModule({
    declarations: [UserComponent],
    imports: [CommonModule, UserRoutingModule, SharedModule]
})
export class UserModule {}