import { Component, OnInit } from '@angular/core'

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { UserService } from "src/app/shared/services/user.service";
import  Swal  from 'sweetalert2'; 
import moment from 'moment';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public Form_: FormGroup;
  public loading = false;
  public submitted = false;
  public error = '';
  public addForm: number = 0;
  public updateForm: any;
  public delForm: number = 0;
  public deleteForm: any;

  divform: boolean = false;
  actions: boolean = true;
  addAction: boolean = false;
  idSelected!: number|undefined;
  indexSelected!: number|undefined;
  today: string;
  constructor(private _formBuilder: FormBuilder, private userService: UserService) { }

  ngOnInit(): void {
  	this.today=moment().format('YYYY-MM-DD');
  	this.Form_ = this._formBuilder.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birth: ['', Validators.required],
      job: ['', Validators.required]
    });
  }

  get f() {
    return this.Form_.controls;
  }
  
  ChooseItem(newItem: any) {
    this.idSelected = newItem.id;
    this.indexSelected = newItem.index;
    this.Form_ = this._formBuilder.group({
      name: [newItem.name, Validators.required],
      username: [newItem.username, Validators.required],
      email: [newItem.email, [Validators.required, Validators.email]],
      birth: [moment.utc(newItem.birth).format('YYYY-MM-DD'), Validators.required],
      job: [newItem.job, Validators.required]
    });
    this.addAction = true;
    this.divform = true;
    this.actions = false;
  }

  add() {
    this.submitted = true;
    if (this.Form_.status === "VALID") {
      this.loading = true;

      let set = {
        name: this.Form_.controls.name.value,
        username: this.Form_.controls.username.value,
        email: this.Form_.controls.email.value,
        birth: this.Form_.controls.birth.value,
        job: this.Form_.controls.job.value
      };
      this.userService.add(set)
      .subscribe(
        res => {
          Swal.fire('Added successfully.', '', 'success');
          this.divform = false;
          this.addAction = false;
          this.loading = false;
          this.submitted = false;
          this.addForm++;
          this.Form_.reset();
        },err => {
          this.loading = false;
          this.submitted = false;
          Swal.fire(err.error.msg || 'An error occurred.', '', 'error');
        }
      );
     
    }
  }
  edit () {
    this.submitted = true;
    if (this.Form_.status === "VALID") {
      this.loading = true;
      let set = {
        id: this.idSelected,
        name: this.Form_.controls.name.value,
        username: this.Form_.controls.username.value,
        email: this.Form_.controls.email.value,
        birth: this.Form_.controls.birth.value,
        job: this.Form_.controls.job.value
      };
      this.userService.edit(set)
      .subscribe(
        res => {
          Swal.fire('It was edited correctly.', '', 'success');
          this.divform = false;
          this.addAction = false;
          this.loading = false;
          this.submitted = false;
          this.idSelected = undefined;
          this.Form_.reset();
          this.updateForm = set;
        },err => {
          this.loading = false;
          this.submitted = false;
          Swal.fire(err.error.msg || 'An error occurred.', '', 'error');
        });
    }
  }
  ChooseItem2(newItem: any)  {
    if (newItem.id) {
    	this.idSelected = newItem.id;
    	this.indexSelected = newItem.index;
    	let id_delete = newItem.id;
    	Swal.fire({
		  title: 'Are you sure you want to delete this record?',
		  showDenyButton: true,
		  showCancelButton: false,
		  confirmButtonText: 'Yes',
		  denyButtonText: 'No',
		}).then((result) => {
		  if (result.isConfirmed) {
		    this.loading = true;
		      this.Form_.reset();
		      this.userService.delete(id_delete)
		      .subscribe((trae: any) => {
		        Swal.fire('It was removed successfully.', '', 'success');
		        this.deleteForm = {index: this.indexSelected, delForm: this.delForm++};
		        this.idSelected = undefined;
		        this.indexSelected = undefined;
		        this.loading = false;
		        this.addAction = false;
		        this.divform = false;
		      });
		  } 
		})
      
    }
  }
  getage(date: string){
  	let v= '';
  	if(date){
	  	let birth=moment(date);
	  	let today=moment();
	  	let y = today.diff(birth,"years")
	  	if(y<=1)
	  		v='('+y+' year)';
	  	else
	  		v='('+y+' years)';
  	}
  	return v;
  }
  resetAll() {
    this.actions = true;
    this.divform = false;
    this.addAction = false;
    this.Form_.reset();
    this.idSelected = undefined;
    this.indexSelected = undefined;
  }
}
