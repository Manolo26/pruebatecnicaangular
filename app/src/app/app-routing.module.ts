import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutHomeComponent } from './layout/layout-home/layout-home.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: "home",
    component: LayoutHomeComponent,
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule)
  },
  {
    path: "user",
    component: LayoutHomeComponent,
    loadChildren: () => import('./user/user.module').then((m) => m.UserModule)
  },
  { path: '**', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
