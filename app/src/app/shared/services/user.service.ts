import { HttpClient,HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUrl: string;
  constructor(
    private _http: HttpClient,
  ) {
    this.apiUrl = environment.apiUrl + "user/";
  }
  get(page: string, items: string, filter: string): Observable<any> {
    return this._http.get<any>(this.apiUrl, { params: new HttpParams().set('page',page).set('items',items).set('filter',filter)});
  }
  gethome(): Observable<any> {
    return this._http.get<any>(this.apiUrl+'home', {});
  }
  add(params: Object): Observable<any> {
    return this._http.post<any>(this.apiUrl, params, {});
  }
  edit(params: Object): Observable<any> {
    return this._http.put<any>(this.apiUrl, params, {});
  }
  delete(id: Number): Observable<any> {
    return this._http.delete<any>(this.apiUrl + id, {});
  }
}
