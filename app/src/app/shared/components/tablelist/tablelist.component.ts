import { Component, Input, OnInit, SimpleChange, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from "src/app/shared/services/user.service";
import { UserElement } from '../../interfaces/user-element.interface';

const DATA: UserElement[] = [];

@Component({
  selector: 'app-tablelist',
  templateUrl: './tablelist.component.html',
  styleUrls: ['./tablelist.component.css']
})
export class TablelistComponent implements OnInit {
  @Output() EdititemEvent = new EventEmitter<any>();
  @Output() DeleteemEvent = new EventEmitter<any>();
  @Input() add: number|undefined;
  @Input() update: any;
  @Input() delete: any;

  total: number = 0;
  page: string = '0';
  numItems: string = '10';
  search: string = "";
  displayedColumns: string[] = ['name', 'job', 'username', 'email', "actions"];

  dataSource = new MatTableDataSource(DATA);
  
  constructor(private userService: UserService) { };

  ngOnInit(): void {
    this.get();
  }
  ngOnChanges(changes: { [property: string]: SimpleChange }){
    //console.log(changes);
      if (changes.add && changes.add.currentValue) {
        this.get();
      } else if (changes.update && changes.update.currentValue !== undefined) {
        this.dataSource.data[Number(changes.update.currentValue.index)].email = changes.update.currentValue.email;
        this.dataSource.data[Number(changes.update.currentValue.index)].name = changes.update.currentValue.name;
        this.dataSource.data[Number(changes.update.currentValue.index)].username = changes.update.currentValue.username;
        this.dataSource.data[Number(changes.update.currentValue.index)].birth = changes.update.currentValue.birth;
        this.dataSource.data[Number(changes.update.currentValue.index)].job = changes.update.currentValue.job;
        this.dataSource._updateChangeSubscription();
        this.update = {};
      } else if (changes.delete && changes.delete.currentValue !== undefined) {
        delete this.dataSource.data[Number(changes.delete.currentValue.index)];
        this.dataSource.data.splice(Number(changes.delete.currentValue.index), 1);
        this.dataSource._updateChangeSubscription();
        this.delete = {};
      }
  }
  pageSelect(page: any) {
    this.page = page.pageIndex;
    this.get();
  }
  get() {
    
    this.userService.get(this.page, this.numItems, this.search)
      .subscribe((trae: any) => {
       this.total = trae.total || 0;
       this.dataSource.data = trae.data;
       this.dataSource._updateChangeSubscription();
      });
  }

  applyFilter(event: Event) {
    this.page='0';
    this.numItems='10';
    setTimeout(() => { this.get(); }, 500);
  }
  addNewItem(value: any, index: number) {
    value.index = index;
    this.EdititemEvent.emit(value);
  }
  DeleteItem(value: any, index: number) {
    value.index = index;
    this.DeleteemEvent.emit(value);
  }
}
