export interface UserElement {
    id?: number;
    name: string;
    username: string;
    email: string;
    birth: string;
    job: string;
}