import { ModuleWithProviders, NgModule, Provider } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatTableModule } from "@angular/material/table";
import { MatToolbarModule } from "@angular/material/toolbar";
import { RouterModule } from "@angular/router";
import { ChartsModule } from "ng2-charts";
import { SidenavComponent } from "./components/sidenav/sidenav.component";
import { TablelistComponent } from "./components/tablelist/tablelist.component";

@NgModule({
    declarations: [
        TablelistComponent,
        SidenavComponent
    ],
    imports: [
        RouterModule,
        MatSidenavModule,
        MatToolbarModule,
        MatListModule,
        MatIconModule,
        MatTableModule,
        MatPaginatorModule,
        FormsModule,
        ReactiveFormsModule,
        ChartsModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule
    ],
    exports: [
        RouterModule,
        MatSidenavModule,
        MatToolbarModule,
        MatListModule,
        MatIconModule,
        MatTableModule,
        MatPaginatorModule,
        FormsModule,
        ReactiveFormsModule,
        ChartsModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        TablelistComponent,
        SidenavComponent
    ]
})
export class SharedModule {
    public static forRoot(): ModuleWithProviders<SharedModule> {
        var providers: Provider[] = [];
        return {
            ngModule: SharedModule,
            providers: [providers],
        };
    }
}